#! /bin/bash
echo ">>> Download the mythril docker image";
docker pull mythril/myth


 
echo ">>> Build folders"
if [ ! -d "build" ]; then
  mkdir 'build';
fi
if [ ! -d "build/flatter" ]; then
  mkdir 'build/flatter';
fi

echo ">>> Flatter contracts"
truffle-flattener ./contracts/marketplace/PositionDispatcher.sol  > ./build/flatter/PositionDispatcher.flat.sol 
truffle-flattener ./contracts/marketplace/Position.sol  > ./build/flatter/Position.flat.sol 

echo ">>> Launch myth analyze"
docker run -v $(pwd):/tmp mythril/myth -v 4 analyze /tmp/build/flatter/PositionDispatcher.flat.sol:PositionDispatcher --solv 0.5.10
docker run -v $(pwd):/tmp mythril/myth -v 4 analyze /tmp/build/flatter/Position.flat.sol:Position --solv 0.5.10
