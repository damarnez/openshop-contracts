const chai = require("chai");
const chaiAsPromised = require("chai-as-promised");
const Bignumber = require("bignumber.js");
chai.use(chaiAsPromised);

const web3 = global.web3;
const {toWei, fromWei, BN} = web3.utils;
const FakeToken = artifacts.require("FakeToken");
const PositionDispatcher = artifacts.require("PositionDispatcher");
const Position = artifacts.require("Position");

contract("Position", accounts => {
  const owner = accounts[0];
  const seller = accounts[1];
  const buyer = accounts[2];
  const randomUser = accounts[3];

  describe("Test Position Contract Logic and Methods", () => {
    let token;
    let positionDispatcher;
    let tokenId;

    const createPosition = async (value = "10") => {
      try {
        const price = web3.utils.toWei(value, "ether"); // TODO: pass to wei
        await positionDispatcher.createPosition(price, token.address, tokenId.toNumber(), {
          from: seller
        });
        const positionEventHistory = await positionDispatcher.getPastEvents("PositionOpened"); // {fromBlock: 0, toBlock: "latest"} put this to get all
        const positionAddress = positionEventHistory[0].returnValues.newPosition;
        return positionAddress;
      } catch (error) {
        console.error(error);
        assert(false);
      }
    };

    describe("Cancel Position", () => {
      beforeEach(async () => {
        try {
          const balance = await web3.eth.getBalance(buyer);
          // Creating fake tokens for test environment.
          token = await FakeToken.deployed();
          await token.mintTo(seller, "http://test.com");
          await token.mintTo(seller, "http://test.com");
          tokenId = await token.tokenOfOwnerByIndex(seller, 0);
          positionDispatcher = await PositionDispatcher.new({from: owner});
          await positionDispatcher.setPositionTemplate(Position.address, {from: owner});
        } catch (error) {
          console.error("Error creation fake token :", error);
        }
      });
      it("Expects position to be cancelled if caller is seller/owner of token and has all necessayr requirements", async () => {
        const positionAddress = await createPosition();
        const position = await Position.at(positionAddress);
        // check state is cancelled
        await position.cancelPosition({from: seller});
        const cancelState = (await position.getState()).toNumber();
        assert(cancelState === 2, "Cancel State");
        //  check owner has ownership of token after cancel
        const updatedTokenId = await token.tokenOfOwnerByIndex(seller, 0);
        assert(tokenId.toNumber() === updatedTokenId.toNumber());
        // check contract does not have ownership of token after cancel
        // check event emited correctly
        const positionEventHistory = await position.getPastEvents("PositionCancelled"); // {fromBlock: 0, toBlock: "latest"} put this to get all
        const positionAddressCancelled = positionEventHistory[0].returnValues.position;
        const positionSellerUpdated = positionEventHistory[0].returnValues.seller;
        assert(seller === positionSellerUpdated, "The seller are the same");
        assert(positionAddress === positionAddressCancelled);
      });
      it("Expects to fail if position is not owned by seller/owner of token", async () => {
        const positionAddress = await createPosition();
        const position = await Position.at(positionAddress);
        try {
          await position.cancelPosition({from: randomUser});
        } catch (err) {
          assert(err.reason === "Caller is not seller");
        }
      });
      it("Expects to fail if state is SOLD", async () => {
        const positionAddress = await createPosition();
        const position = await Position.at(positionAddress);
        await token.approve(positionAddress, tokenId.toNumber(), {from: seller});
        await position.publishPosition({from: seller});
        // sell the token
        try {
          const price = web3.utils.toWei("10", "ether");
          await position.buyPosition({from: buyer, value: price});
        } catch (err) {
          assert(!err);
        }
        try {
          await position.cancelPosition({from: seller});
        } catch (err) {
          assert(err.reason === "Position already sold");
        }
        // The seller is not the owner of the token
        const updatedTokenId = await token.tokenOfOwnerByIndex(seller, 0);
        assert(tokenId.toNumber() !== updatedTokenId.toNumber());
      });
    });
    describe("Publish Position", () => {
      beforeEach(async () => {
        try {
          token = await FakeToken.deployed();
          await token.mintTo(seller, "http://test.com");
          await token.mintTo(seller, "http://test.com");
          tokenId = await token.tokenOfOwnerByIndex(seller, 0);
          positionDispatcher = await PositionDispatcher.new({from: owner});
          await positionDispatcher.setPositionTemplate(Position.address, {from: owner});
        } catch (error) {
          console.error("Error creation fake token :", error);
        }
      });
      it("Expects to publish a position when all conditions apply", async () => {
        const positionAddress = await createPosition();
        const position = await Position.at(positionAddress);
        await token.approve(positionAddress, tokenId.toNumber(), {from: seller});
        await position.publishPosition({from: seller});
        const positionEventHistory = await position.getPastEvents("PositionPublished"); // {fromBlock: 0, toBlock: "latest"} put this to get all
        const positionAddressPublish = positionEventHistory[0].returnValues.position;
        const positionSellerUpdated = positionEventHistory[0].returnValues.seller;
        const positionTokenIdUpdated = positionEventHistory[0].returnValues.tokenId;

        assert(seller === positionSellerUpdated, "The seller are not the same");
        assert(positionAddress === positionAddressPublish, "The position address is not the same");
        assert(
          tokenId.toNumber() === parseInt(positionTokenIdUpdated),
          "The tokenId is not the same"
        );
      });
      it("Expects to fail when the publisher is not the seller", async () => {
        const positionAddress = await createPosition();
        const position = await Position.at(positionAddress);
        await token.approve(positionAddress, tokenId.toNumber(), {from: seller});
        try {
          await position.publishPosition({from: randomUser});
        } catch (err) {
          assert(err.reason === "Caller is not seller");
        }
      });
      it("Expects to fail when position is already published", async () => {
        const positionAddress = await createPosition();
        const position = await Position.at(positionAddress);
        await token.approve(positionAddress, tokenId.toNumber(), {from: seller});
        await position.publishPosition({from: seller});
        try {
          await position.publishPosition({from: seller});
        } catch (err) {
          assert(err.reason === "Position already published");
        }
      });
      it("Expects to fail when the position is not approved to use the token", async () => {
        const positionAddress = await createPosition();
        const position = await Position.at(positionAddress);
        try {
          await position.publishPosition({from: seller});
        } catch (err) {
          assert(err.reason === "This contract is not approved to use this token");
        }
      });
    });
    describe("Buy Position", () => {
      beforeEach(async () => {
        try {
          token = await FakeToken.deployed();
          await token.mintTo(seller, "http://test.com");
          await token.mintTo(seller, "http://test.com");
          tokenId = await token.tokenOfOwnerByIndex(seller, 0);
          positionDispatcher = await PositionDispatcher.new({from: owner});
          await positionDispatcher.setPositionTemplate(Position.address, {from: owner});
        } catch (error) {
          console.error("Error creation fake token :", error);
        }
      });
      it("Expects to buy position if everything is as expected", async () => {
        const positionAddress = await createPosition();
        const position = await Position.at(positionAddress);
        await token.approve(positionAddress, tokenId.toNumber(), {from: seller});
        await position.publishPosition({from: seller});
        const beforeBalanceBuyer = await web3.eth.getBalance(buyer);
        const beforeBalanceSeller = await web3.eth.getBalance(seller);
        // sell the token
        try {
          const price = web3.utils.toWei("10", "ether");
          await position.buyPosition({from: buyer, value: price});
        } catch (err) {
          assert(!err, "Unexpected error when try to buy");
        }
        // Check correct balance of buyer
        // Check that buyer owns token
        // Check that seller does not have token any more
        const ownerOfToken = await token.ownerOf(tokenId.toNumber());
        assert(ownerOfToken !== seller, "TokenId are not to be the seller");
        assert(ownerOfToken === buyer, "TokenId need to be the buyer");
        // Check correct balance of seller
        const afterBalanceBuyer = await web3.eth.getBalance(buyer);

        assert(
          new Bignumber(beforeBalanceBuyer).gt(new Bignumber(afterBalanceBuyer)),
          "Balance of the buyer are not correct"
        );
        const afterBalanceSeller = await web3.eth.getBalance(seller);
        assert(
          new Bignumber(afterBalanceSeller).gt(new Bignumber(beforeBalanceSeller)),
          "Balance of the seller are not correct"
        );
        // Check contract does not have permisions to operate with token
        const allowed = await token.getApproved(tokenId.toNumber());
        assert(allowed === "0x0000000000000000000000000000000000000000", "Is approved for someone");

        // Check state is SOLD
        const soldState = (await position.getState()).toNumber();
        assert(soldState === 3, "Sold State");
        // Check event is emited
        const positionEventHistory = await position.getPastEvents("PositionBought"); // {fromBlock: 0, toBlock: "latest"} put this to get all
        const positionAddressPublish = positionEventHistory[0].returnValues.position;
        const positionSellerUpdated = positionEventHistory[0].returnValues.seller;
        const positionTokenIdUpdated = positionEventHistory[0].returnValues.tokenId;
        assert(seller === positionSellerUpdated, "The seller are not the same");
        assert(positionAddress === positionAddressPublish, "The position address is not the same");
        assert(
          tokenId.toNumber() === parseInt(positionTokenIdUpdated),
          "The tokenId is not the same"
        );
      });
      it("Expects to buy position with a position with price 0", async () => {
        const positionAddress = await createPosition("0");
        const position = await Position.at(positionAddress);
        await token.approve(positionAddress, tokenId.toNumber(), {from: seller});
        await position.publishPosition({from: seller});
        const beforeBalanceBuyer = await web3.eth.getBalance(buyer);
        const beforeBalanceSeller = await web3.eth.getBalance(seller);
        // sell the token
        try {
          const price = web3.utils.toWei("0", "ether");
          await position.buyPosition({from: buyer, value: price});
        } catch (err) {
          assert(!err, "Unexpected error when try to buy");
        }
        // Check correct balance of buyer
        // Check that buyer owns token
        // Check that seller does not have token any more
        const ownerOfToken = await token.ownerOf(tokenId.toNumber());
        assert(ownerOfToken !== seller, "TokenId are not to be the seller");
        assert(ownerOfToken === buyer, "TokenId need to be the buyer");
        // Check correct balance of seller
        const afterBalanceBuyer = await web3.eth.getBalance(buyer);

        assert(
          new Bignumber(beforeBalanceBuyer).gte(new Bignumber(afterBalanceBuyer)),
          "Balance of the buyer are not correct"
        );
        const afterBalanceSeller = await web3.eth.getBalance(seller);
        assert(
          new Bignumber(afterBalanceSeller).eq(new Bignumber(beforeBalanceSeller)),
          "Balance of the seller are not correct"
        );
        // Check contract does not have permisions to operate with token
        const allowed = await token.getApproved(tokenId.toNumber());
        assert(allowed === "0x0000000000000000000000000000000000000000", "Is approved for someone");

        // Check state is SOLD
        const soldState = (await position.getState()).toNumber();
        assert(soldState === 3, "Sold State");
        // Check event is emited
        const positionEventHistory = await position.getPastEvents("PositionBought"); // {fromBlock: 0, toBlock: "latest"} put this to get all
        const positionAddressPublish = positionEventHistory[0].returnValues.position;
        const positionSellerUpdated = positionEventHistory[0].returnValues.seller;
        const positionTokenIdUpdated = positionEventHistory[0].returnValues.tokenId;
        assert(seller === positionSellerUpdated, "The seller are not the same");
        assert(positionAddress === positionAddressPublish, "The position address is not the same");
        assert(
          tokenId.toNumber() === parseInt(positionTokenIdUpdated),
          "The tokenId is not the same"
        );
      });
      it("Expects to fail when position is not public", async () => {
        const positionAddress = await createPosition();
        const position = await Position.at(positionAddress);
        await token.approve(positionAddress, tokenId.toNumber(), {from: seller});

        // sell the token
        try {
          const price = web3.utils.toWei("10", "ether");
          await position.buyPosition({from: buyer, value: price});
        } catch (err) {
          assert(err.reason === "Position state is not PUBLIC", "Position state is not PUBLIC");
        }
      });
      it("Expects to fail when value is less than position price", async () => {
        const positionAddress = await createPosition();
        const position = await Position.at(positionAddress);
        await token.approve(positionAddress, tokenId.toNumber(), {from: seller});
        await position.publishPosition({from: seller});
        // sell the token
        try {
          const price = web3.utils.toWei("5", "ether");
          await position.buyPosition({from: buyer, value: price});
        } catch (err) {
          assert(
            err.reason === "The value sent is smaller than price",
            "The value sent is smaller than price"
          );
        }
      });
      it("Expects to fail when contract is not approved to operate with the token", async () => {
        const positionAddress = await createPosition();
        const position = await Position.at(positionAddress);
        try {
          const price = web3.utils.toWei("10", "ether");
          await position.buyPosition({from: buyer, value: price});
        } catch (err) {
          assert(err.reason, "Position state is not PUBLIC");
        }
      });
      it("Expects to fail when buyer has less balance than the price", async () => {
        const balanceBuyer = await web3.eth.getBalance(buyer);
        const positionAddress = await createPosition(balanceBuyer + "00");
        const position = await Position.at(positionAddress);
        await token.approve(positionAddress, tokenId.toNumber(), {from: seller});
        await position.publishPosition({from: seller});
        // sell the token
        try {
          const price = web3.utils.toWei(balanceBuyer + "00", "ether");
          await position.buyPosition({from: buyer, value: price});
        } catch (err) {
          assert(
            err.reason === "Buyer does not have enought money",
            "Buyer does not have enought money"
          );
        }
      });
      it("Expects to fail when the contract is approved to publish but disaproved later for sell", async () => {
        const positionAddress = await createPosition();
        const position = await Position.at(positionAddress);
        await token.approve(positionAddress, tokenId.toNumber(), {from: seller});
        await position.publishPosition({from: seller});
        // We publish again and allow other contract
        const newPositionAddress = await createPosition();
        await token.approve(newPositionAddress, tokenId.toNumber(), {from: seller});
        // sell the token
        try {
          const price = web3.utils.toWei("10", "ether");
          await position.buyPosition({from: buyer, value: price});
        } catch (err) {
          assert(
            err.reason === "This contract is not approved to use this token",
            "This contract is not approved to use this token"
          );
        }
      });
    });
    describe("Get State", () => {
      beforeEach(async () => {
        try {
          token = await FakeToken.deployed();
          await token.mintTo(seller, "http://test.com");
          await token.mintTo(seller, "http://test.com");
          tokenId = await token.tokenOfOwnerByIndex(seller, 0);
          positionDispatcher = await PositionDispatcher.new({from: owner});
          await positionDispatcher.setPositionTemplate(Position.address, {from: owner});
        } catch (error) {
          console.error("Error creation fake token :", error);
        }
      });
      it("Expects the default state is 0", async () => {
        const positionAddress = await createPosition();
        const position = await Position.at(positionAddress);
        const positionState = await position.getState();
        assert(positionState.toNumber() === 0, "Wrong initial state");
      });
    });
  });
});
