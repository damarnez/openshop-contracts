const chai = require("chai");
const chaiAsPromised = require("chai-as-promised");
chai.use(chaiAsPromised);

const web3 = global.web3;
const {toWei, fromWei, BN} = web3.utils;
const FakeToken = artifacts.require("FakeToken");
const PositionDispatcher = artifacts.require("PositionDispatcher");
const Position = artifacts.require("Position");

contract("Token transaction", accounts => {
  const owner = accounts[0];
  const seller = accounts[1];
  const buyer = accounts[2];

  describe("Test a full circle flow", () => {
    let token;
    let positionDispatcher;
    const fee = web3.utils.toWei("1", "ether");
    let tokenId;
    beforeEach(async () => {
      try {
        const balance = await web3.eth.getBalance(buyer);
        // Creating fake tokens for test environment.
        token = await FakeToken.deployed();
        await token.mintTo(seller, "http://test.com");
        await token.mintTo(seller, "http://test.com");
        tokenId = await token.tokenOfOwnerByIndex(seller, 0);
        positionDispatcher = await PositionDispatcher.new({from: owner});
        await positionDispatcher.setPositionTemplate(Position.address, {from: owner});
      } catch (error) {
        console.error("Error creation fake token :", error);
      }
    });
    it("Expects the positions was created correctly", async () => {
      try {
        const price = web3.utils.toWei("10", "ether"); // TODO: pass to wei
        await positionDispatcher.createPosition(price, token.address, tokenId.toNumber(), {
          from: seller
        });
        const positionEventHistory = await positionDispatcher.getPastEvents("PositionOpened"); // {fromBlock: 0, toBlock: "latest"} put this to get all
        const positionAddress = positionEventHistory[0].returnValues.newPosition;
        assert(positionAddress !== "", "Position created");
        // TODO: create all checks
      } catch (error) {
        console.error(error);
        assert(false);
      }
    });
    it("Expects the states updated on contract Position", async () => {
      try {
        const price = web3.utils.toWei("10", "ether"); // TODO: pass to wei
        await positionDispatcher.createPosition(price, token.address, tokenId.toNumber(), {
          from: seller
        });
        // Get the event on creation
        const positionEventHistory = await positionDispatcher.getPastEvents("PositionOpened"); // {fromBlock: 0, toBlock: "latest"} put this to get all
        const positionAddress = positionEventHistory[0].returnValues.newPosition;
        const position = await Position.at(positionAddress);
        // give approval
        await token.approve(positionAddress, tokenId.toNumber(), {from: seller});
        //Check state
        const currentState = (await position.getState()).toNumber();
        assert(currentState === 0, "Review State");
        // Publish the contract
        await position.publishPosition({from: seller});
        const updatedState = (await position.getState()).toNumber();
        assert(updatedState === 1, "Public State");
        // TODO: create all checks
      } catch (error) {
        console.error(error);
        assert(false);
      }
    });
    it("Expects to not be able to buy if seller cancels position", async () => {
      const price = web3.utils.toWei("10", "ether"); // TODO: pass to wei
      await positionDispatcher.createPosition(price, token.address, tokenId.toNumber(), {
        from: seller
      });
      // Get the event on creation
      const positionEventHistory = await positionDispatcher.getPastEvents("PositionOpened"); // {fromBlock: 0, toBlock: "latest"} put this to get all
      const positionAddress = positionEventHistory[0].returnValues.newPosition;
      const position = await Position.at(positionAddress);
      // give approval
      await token.approve(positionAddress, tokenId.toNumber(), {from: seller});
      //Check state
      const currentState = (await position.getState()).toNumber();
      assert(currentState === 0, "Review State");
      // Publish the contract
      await position.publishPosition({from: seller});
      const publishState = (await position.getState()).toNumber();
      assert(publishState === 1, "Public State");
      await position.cancelPosition({from: seller});
      const cancelState = (await position.getState()).toNumber();
      assert(cancelState === 2, "Cancel State");
      try {
        await position.buyPosition({from: buyer});
      } catch (err) {
        assert(err.reason === "Position state is not PUBLIC");
      }
    });

    it("Expects if the position is cancelled the seller owns the token", async () => {
      const price = web3.utils.toWei("10", "ether"); // TODO: pass to wei
      await positionDispatcher.createPosition(price, token.address, tokenId.toNumber(), {
        from: seller
      });
      // Get the event on creation
      const positionEventHistory = await positionDispatcher.getPastEvents("PositionOpened"); // {fromBlock: 0, toBlock: "latest"} put this to get all
      const positionAddress = positionEventHistory[0].returnValues.newPosition;
      const position = await Position.at(positionAddress);
      // give approval
      await token.approve(positionAddress, tokenId.toNumber(), {from: seller});
      //Check state
      const currentState = (await position.getState()).toNumber();
      assert(currentState === 0, "Review State");
      // Publish the contract
      await position.publishPosition({from: seller});
      const publishState = (await position.getState()).toNumber();
      await position.cancelPosition({from: seller});
      const cancelState = (await position.getState()).toNumber();
      try {
        await position.buyPosition({from: buyer});
      } catch (err) {
        assert(err.reason === "Position state is not PUBLIC");
      }

      updatedTokenId = await token.tokenOfOwnerByIndex(seller, 0);
      assert(tokenId.toNumber() === updatedTokenId.toNumber());
    });
  });
});
