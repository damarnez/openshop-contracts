const chai = require("chai");
const chaiAsPromised = require("chai-as-promised");
const truffleAssert = require("truffle-assertions");
chai.use(chaiAsPromised);

const web3 = global.web3;
const {toWei, fromWei, BN} = web3.utils;
const FakeToken = artifacts.require("FakeToken");
const PositionDispatcher = artifacts.require("PositionDispatcher");
const Position = artifacts.require("Position");

contract("PositionDispatcher", accounts => {
  const owner = accounts[0];
  const seller = accounts[1];
  const buyer = accounts[2];
  const otherOwner = accounts[3];

  describe("Test the position dispatcher contract", () => {
    describe("update marketplace address", async () => {
      let positionDispatcher;
      beforeEach(async () => {
        positionDispatcher = await PositionDispatcher.new({from: owner});
        const positionDispatcherEventHistory = await positionDispatcher.getPastEvents(
          "PositionDispatcherCreated"
        );
        await positionDispatcher.setPositionTemplate(Position.address, {from: owner});
        const positionDispatcherAddress =
          positionDispatcherEventHistory[0].returnValues.positionDispatcher;
        assert(positionDispatcherAddress === positionDispatcher.address);
      });
      it("Expects to change the marketplace addres whe caller is owner", async () => {
        await positionDispatcher.updateMarketPlaceAddress(otherOwner, {from: owner});
        const mpAddress = await positionDispatcher.marketPlaceAddress();
        expect(mpAddress).equal(otherOwner);

        const positionDispatcherEventHistory = await positionDispatcher.getPastEvents(
          "UpdateMPAddress"
        );
        const marketPlaceAddress = positionDispatcherEventHistory[0].returnValues.mkpAddress;
        expect(marketPlaceAddress).equal(mpAddress);
      });
      it("Expects to fail when caller is not woner", async () => {
        await truffleAssert.fails(
          positionDispatcher.updateMarketPlaceAddress(otherOwner, {from: otherOwner}),
          truffleAssert.ErrorType.REVERT,
          "caller is not the owner"
        );
      });
    });
    describe("Update MP fee", async () => {
      beforeEach(async () => {
        positionDispatcher = await PositionDispatcher.new({from: owner});
        const positionDispatcherEventHistory = await positionDispatcher.getPastEvents(
          "PositionDispatcherCreated"
        );
        await positionDispatcher.setPositionTemplate(Position.address, {from: owner});
        const positionDispatcherAddress =
          positionDispatcherEventHistory[0].returnValues.positionDispatcher;
        assert(positionDispatcherAddress === positionDispatcher.address);
      });
      it("Expects to update the fee when caller is owner", async () => {
        const newFee = web3.utils.toWei("10", "ether"); // TODO: pass to wei
        await positionDispatcher.updateMPFee(newFee, {from: owner});
        const mpFee = await positionDispatcher.MPFee();
        expect(Number(mpFee)).equal(Number(newFee));

        const positionDispatcherEventHistory = await positionDispatcher.getPastEvents(
          "UpdateMPFee"
        );
        const eventNewFee = positionDispatcherEventHistory[0].returnValues.newFee;
        expect(Number(newFee)).equal(Number(eventNewFee));
      });
      it("Expects to fail when caller is not owner", async () => {
        const newFee = web3.utils.toWei("10", "ether"); // TODO: pass to wei
        await truffleAssert.fails(
          positionDispatcher.updateMPFee(newFee, {from: otherOwner}),
          truffleAssert.ErrorType.REVERT,
          "caller is not the owner"
        );
      });
    });
    describe("Create Position", async () => {
      let token;
      beforeEach(async () => {
        positionDispatcher = await PositionDispatcher.new({from: owner});
        const positionDispatcherEventHistory = await positionDispatcher.getPastEvents(
          "PositionDispatcherCreated"
        );
        await positionDispatcher.setPositionTemplate(Position.address, {from: owner});
        const positionDispatcherAddress =
          positionDispatcherEventHistory[0].returnValues.positionDispatcher;
        assert(positionDispatcherAddress === positionDispatcher.address);

        token = await FakeToken.deployed();
        await token.mintTo(seller, "http://test.com");
      });
      it("Expects to create Position if all is well", async () => {
        const price = web3.utils.toWei("10", "ether");
        const tokenAddress = token.address;
        const tokenId = await token.tokenOfOwnerByIndex(seller, 0);
        const position = await positionDispatcher.createPosition(price, tokenAddress, tokenId, {
          from: seller
        });

        const positionDispatcherEventHistory = await positionDispatcher.getPastEvents(
          "PositionOpened"
        );
        const {
          positionDispatcher: eventPositionDispatcher,
          newPosition,
          seller: sellerEvent,
          tokenAddress: eventTokenAddress,
          tokenId: eventTokenId,
          price: eventPrice
        } = positionDispatcherEventHistory[0].returnValues;
        expect(Number(price)).equal(Number(eventPrice));
        expect(seller).equal(sellerEvent);
        expect(Number(tokenId)).equal(Number(eventTokenId));
        expect(positionDispatcher.address).equal(eventPositionDispatcher);
        expect(tokenAddress).equal(eventTokenAddress);

        const isPosition = await positionDispatcher.isAddressAPosition(newPosition);
        expect(isPosition).equal(true);
      });
      it("Expects to fail when seller is not the owner of the token", async () => {
        const price = web3.utils.toWei("10", "ether");
        const tokenAddress = token.address;
        const tokenId = await token.tokenOfOwnerByIndex(seller, 0);

        await truffleAssert.fails(
          positionDispatcher.createPosition(price, tokenAddress, tokenId, {
            from: otherOwner
          }),
          truffleAssert.ErrorType.REVERT,
          "Seller is not the token owner"
        );
      });
    });
    describe("Is address Position", async () => {
      let token;
      let position;
      beforeEach(async () => {
        positionDispatcher = await PositionDispatcher.new({from: owner});
        await positionDispatcher.setPositionTemplate(Position.address, {from: owner});

        token = await FakeToken.deployed();
        await token.mintTo(seller, "http://test.com");
        const price = web3.utils.toWei("10", "ether");
        const tokenAddress = token.address;
        const tokenId = await token.tokenOfOwnerByIndex(seller, 0);
        await positionDispatcher.createPosition(price, tokenAddress, tokenId, {
          from: seller
        });
        const positionDispatcherEventHistory = await positionDispatcher.getPastEvents(
          "PositionOpened"
        );
        const {newPosition} = positionDispatcherEventHistory[0].returnValues;
        position = newPosition;
      });
      it("Expects to return true if address is a correct position", async () => {
        const isAddress = await positionDispatcher.isAddressAPosition(position);
        expect(isAddress).equal(true);
      });
      it("Expects to return false if address is not a position", async () => {
        const isAddress = await positionDispatcher.isAddressAPosition(seller);
        expect(isAddress).equal(false);
      });
    });
  });
});
