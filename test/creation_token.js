const chai = require("chai");
const chaiAsPromised = require("chai-as-promised");
chai.use(chaiAsPromised);

const FakeToken = artifacts.require("FakeToken");

contract("Create Token", function(accounts) {
  const owner = accounts[0];
  const user = accounts[1];
  const admin = accounts[2];

  xit("Create one Token", async () => {
    try {
      const token = await FakeToken.deployed();

      token.mintTo(user, "http://test.com"); // will mint a new token
      const total = await token.totalSupply(); // check token's total supply
      assert(total === 1);
    } catch (error) {
      console.log("error====> ", error);
      assert(false);
    }
  });
});
