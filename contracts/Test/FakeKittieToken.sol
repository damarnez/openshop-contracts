pragma solidity 0.5.10;

import "openzeppelin-solidity/contracts/token/ERC721/ERC721Full.sol";
import "openzeppelin-solidity/contracts/ownership/Ownable.sol";

/**
 * @title KauriBadges
 * KauriBadges - non-fungible badges
 */
contract FakeKittieToken is ERC721Full, Ownable {
    constructor() public ERC721Full("FakeKittieToken", "CK") {}
    mapping(uint256 => address) public kittyIndexToApproved; // This is the list of approveds
    /**
    * @dev Mints a token to an address with a tokenURI.
    * @param _to address of the future owner of the token
    * @param _tokenURI token URI for the token
    */
    function mintTo(address _to, string memory _tokenURI) public onlyOwner returns (uint256) {
        uint256 newTokenId = _getNextTokenId();
        _mint(_to, newTokenId);
        _setTokenURI(newTokenId, _tokenURI);

        return newTokenId;
    }
    function xapprove(address _to, uint256 _tokenId) public {
        kittyIndexToApproved[_tokenId] = _to;

    }
    function transferFrom(address _from, address _to, uint256 _tokenId) public {
        delete kittyIndexToApproved[_tokenId];
        transferFrom(_from, _to, _tokenId);
    }
    /**
    * @dev calculates the next token ID based on totalSupply
    * @return uint256 for the next token ID
    */
    function _getNextTokenId() private view returns (uint256) {
        return totalSupply().add(1);
    }
}
