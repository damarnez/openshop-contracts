pragma solidity 0.5.10;

import "@openzeppelin/contracts/math/SafeMath.sol";
import "@openzeppelin/contracts/utils/ReentrancyGuard.sol";
import "./interfaces/IPosition.sol";
import "./library/Wrapper721.sol";

contract Position is IPosition, ReentrancyGuard {
    using SafeMath for uint256;

    address public token721;
    bool private isTemplate;

    uint256 public positionCreated;
    uint256 public price;
    uint256 public MPFee;
    uint256 public positionFee;

    address payable public seller;
    address payable public marketPlaceAddress;
    address public tokenAddress;
    uint256 public tokenId;

    uint256 constant ONE_HUNDRED = 100e18;

    event PositionCreated(
        address indexed position,
        address indexed seller,
        uint256 indexed tokenId,
        uint256 price,
        address tokenAddress,
        address marketPlaceAddress,
        uint256 timestamp
    );

    event PositionBought(
        address indexed position,
        address indexed seller,
        uint256 indexed tokenId,
        address tokenAddress,
        uint256 price,
        address buyer,
        uint256 mktFee,
        uint256 sellerProfit,
        uint256 timestamp
    );

    modifier notTemplate() {
        require(isTemplate == false, "you cant call template contract");
        _;
    }

    constructor() public {
        isTemplate = true;
    }

    function init(
        address payable _seller,
        uint256 _MPFee,
        uint256 _price,
        address _tokenAddress,
        uint256 _tokenId,
        address payable _marketPlaceAddress
    ) external notTemplate returns (bool) {
        positionCreated = block.timestamp;

        seller = _seller;
        MPFee = _MPFee;
        price = _price;
        tokenAddress = _tokenAddress;
        tokenId = _tokenId;

        marketPlaceAddress = _marketPlaceAddress;

        token721 = tokenAddress;

        emit PositionCreated(
            address(this),
            seller,
            tokenId,
            price,
            tokenAddress,
            marketPlaceAddress,
            block.timestamp
        );

        return true;
    }

    function isTemplateContract() external view returns (bool) {
        return isTemplate;
    }

    function buyPosition() external payable nonReentrant notTemplate {
        address approved = Wrapper721.getApproved(token721, tokenId);
        require(approved == address(this), "This contract is not approved to use this token");
        require(address(msg.sender).balance >= price, "Buyer does not have enought money");
        require(msg.value >= price, "The value sent is smaller than price");
        uint256 mktFee = price.mul(MPFee).div(ONE_HUNDRED);
        uint256 sellerProfit = price.sub(mktFee);

        seller.transfer(sellerProfit);

        marketPlaceAddress.transfer(mktFee);

        Wrapper721.safeTransferFrom(token721, seller, msg.sender, tokenId);

        emit PositionBought(
            address(this),
            seller,
            tokenId,
            tokenAddress,
            price,
            msg.sender,
            mktFee,
            sellerProfit,
            block.timestamp
        );
    }
}
