pragma solidity 0.5.10;

interface I721Kitty {
    function ownerOf(uint256 _tokenId) external view returns (address owner);
    function transfer(address _to, uint256 _tokenId) external;
    function transferFrom(address _from, address _to, uint256 _tokenId) external;
    function kittyIndexToApproved(uint256 tokenId) external view returns (address owner);
    // mapping(uint256 => address) public kittyIndexToApproved;
}

interface I721 {
    function ownerOf(uint256 tokenId) external view returns (address owner);
    function safeTransferFrom(address from, address to, uint256 tokenId) external;
    function getApproved(uint256 tokenId) external view returns (address operator);
}

interface I721Meta {
    function symbol() external view returns (string memory);
}
library Wrapper721 {
    function safeTransferFrom(address _token, address _from, address _to, uint256 _tokenId)
        external
    {
        if (isIssuedToken(_token)) {
            I721Kitty(_token).transferFrom(_from, _to, _tokenId);
        } else {
            I721(_token).safeTransferFrom(_from, _to, _tokenId);
        }

    }
    function getApproved(address _token, uint256 _tokenId) external view returns (address) {
        if (isIssuedToken(_token)) {
            return I721Kitty(_token).kittyIndexToApproved(_tokenId);
        } else {
            return I721(_token).getApproved(_tokenId);
        }
    }
    function ownerOf(address _token, uint256 _tokenId) public view returns (address owner) {
        if (isIssuedToken(_token)) {
            return I721Kitty(_token).ownerOf(_tokenId);
        } else {
            return I721(_token).ownerOf(_tokenId);
        }
    }
    function isIssuedToken(address _token) private view returns (bool) {
        return (keccak256(abi.encodePacked((I721Meta(_token).symbol()))) ==
            keccak256(abi.encodePacked(("CK"))));
    }

}
