pragma solidity 0.5.10;

interface IPositionDispatcher {
    event PositionDispatcherCreated(
        address indexed positionDispatcher,
        uint256 timestamp,
        address mpAddress,
        uint256 mpFee
    );
    event UpdateMPFee(address indexed positionDispatcher, uint256 newFee);
    event PositionOpened(
        address indexed positionDispatcher,
        address indexed newPosition,
        uint256 timestamp,
        address seller,
        address tokenAddress,
        uint256 tokenId,
        address marketPlaceAddress,
        uint256 price
    );
    event UpdateMPAddress(address indexed positionDispatcher, address mkpAddress);
    event UpdatedPositionContract(address indexed positionDispatcher, address positionContract);

    function updateMarketPlaceAddress(address payable newMarketPlaceAddress) external;

    function updateMPFee(uint256 newFee) external;

    function setPositionTemplate(address newPositionTemplate) external;

    function createPosition(
        uint256 price,
        address tokenAddress,
        uint256 tokenId
    ) external returns (address);

    function isAddressAPosition(address positionAddress) external view returns (bool);

    function isCloned(address target, address query) external view returns (bool result);
}
