pragma solidity 0.5.10;

interface IPosition {
    event PositionCreated(
        address indexed position,
        address indexed seller,
        uint256 indexed tokenId,
        uint256 price,
        address tokenAddress,
        address marketPlaceAddress,
        uint256 timestamp
    );

    event PositionBought(
        address indexed position,
        address indexed seller,
        uint256 indexed tokenId,
        uint256 tokenAddress,
        uint256 price,
        address buyer,
        uint256 mktFee,
        uint256 sellerProfit,
        uint256 timestamp
    );

    function init(
        address payable _seller,
        uint256 _MPFee,
        uint256 _price,
        address _tokenAddress,
        uint256 _tokenId,
        address payable _marketPlaceAddress
    ) external returns (bool);

    function isTemplateContract() external view returns (bool);

    function buyPosition() external payable;
}
