pragma solidity 0.5.10;

import "@openzeppelin/contracts/ownership/Ownable.sol";
import "./interfaces/IPosition.sol";
import "./interfaces/IPositionDispatcher.sol";
import "./library/Wrapper721.sol";
import "./CloneFactory.sol";

contract PositionDispatcher is IPositionDispatcher, CloneFactory, Ownable {
    uint256 public contractCreated;
    uint256 public MPFee;

    address payable public marketPlaceAddress;
    address public positionTemplate;

    mapping(address => bool) public isPosition;

    event PositionDispatcherCreated(
        address indexed positionDispatcher,
        uint256 timestamp,
        address mpAddress,
        uint256 mpFee
    );
    event UpdateMPFee(address indexed positionDispatcher, uint256 newFee);
    event PositionOpened(
        address indexed positionDispatcher,
        address indexed newPosition,
        uint256 timestamp,
        address seller,
        address tokenAddress,
        uint256 tokenId,
        address marketPlaceAddress,
        uint256 price
    );
    event UpdateMPAddress(address indexed positionDispatcher, address mkpAddress);
    event UpdatedPositionContract(address indexed positionDispatcher, address positionContract);

    constructor() public {
        contractCreated = block.timestamp;
        marketPlaceAddress = 0x4f86A75764710683DAC3833dF49c72de3ec65465;
        MPFee = 1e18;
        emit PositionDispatcherCreated(address(this), block.timestamp, marketPlaceAddress, MPFee);
    }

    function updateMarketPlaceAddress(address payable newMarketPlaceAddress) external onlyOwner {
        marketPlaceAddress = newMarketPlaceAddress;
        emit UpdateMPAddress(address(this), marketPlaceAddress);
    }

    function updateMPFee(uint256 newFee) external onlyOwner {
        MPFee = newFee;
        emit UpdateMPFee(address(this), newFee);
    }

    function setPositionTemplate(address newPositionTemplate) external onlyOwner {
        positionTemplate = newPositionTemplate;
        emit UpdatedPositionContract(address(this), positionTemplate);
    }

    function createPosition(
        uint256 price,
        address tokenAddress,
        uint256 tokenId
    ) external returns (address) {
        require(
            sellerIsTokenOwner(msg.sender, tokenAddress, tokenId),
            "Seller is not the token owner"
        );

        require(positionTemplate != address(0), "position contract needs to be set");

        address positionContract = createClone((positionTemplate));

        require(
            IPosition(positionContract).init(
                msg.sender,
                MPFee,
                price,
                tokenAddress,
                tokenId,
                marketPlaceAddress
            ),
            "Failed to init position"
        );

        isPosition[positionContract] = true;

        emit PositionOpened(
            address(this),
            positionContract,
            block.timestamp,
            msg.sender,
            tokenAddress,
            tokenId,
            marketPlaceAddress,
            price
        );

        return positionContract;
    }

    function isAddressAPosition(address positionAddress) external view returns (bool) {
        return isPosition[positionAddress];
    }

    function sellerIsTokenOwner(
        address seller,
        address tokenAddress,
        uint256 tokenId
    ) internal view returns (bool) {
        address tokenOwner = Wrapper721.ownerOf(tokenAddress, tokenId);
        return tokenOwner == seller;
    }

    function isCloned(address target, address query) external view returns (bool result) {
        return isClone(target, query);
    }
}
