const Position = artifacts.require("Position");
const PositionDispatcher = artifacts.require("PositionDispatcher");
const FakeToken = artifacts.require("FakeToken");
const FakeKittieToken = artifacts.require("FakeKittieToken");
const Wrapper721 = artifacts.require("Wrapper721");
const Web3 = require("web3");

const loadWeb3One = () => {
  web3 = new Web3(web3.currentProvider);
};

const migrationInt = async (deployer, network, accounts) => {
  const deployerAddress = accounts[0];

  console.log("||-- Deploy Wrapper721 --||");
  await deployer.deploy(Wrapper721, {
    from: deployerAddress
  });

  // Link the contracts
  console.log("||-- Link Wrapper721 with PositionDispatcher && Position --||");
  deployer.link(Wrapper721, [PositionDispatcher, Position]);

  console.log("||-- Deploy Position Template Contract --||");
  await deployer.deploy(Position, {from: deployerAddress});

  // if (network !== 1 && network !== 42) {
  //   await deployer.deploy(FakeToken, {from: deployerAddress});
  //   await deployer.deploy(FakeKittieToken, {from: deployerAddress});
  // }

  console.log("||-- Deploy Position Dispatcher --||");
  await deployer.deploy(PositionDispatcher, {from: deployerAddress});

  const positionDispatcher = await PositionDispatcher.at(PositionDispatcher.address);

  await positionDispatcher.setPositionTemplate(Position.address, {from: deployerAddress});
};

module.exports = async (deployer, network, accounts) => {
  console.log(`Deploying in network:: ${network}`);

  loadWeb3One();
  deployer.then(async () => {
    try {
      await migrationInt(deployer, network, accounts);
    } catch (err) {
      // Prettier error output
      console.log(err);
      throw err;
    }
  });
};
